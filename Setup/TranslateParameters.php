<?php

namespace Setup;

class TranslateParameters
{
    /**
     *
     * @var string
     */
    private $version;

    /**
     *
     * @var string
     */
    private $sourceFile;

    /**
     *
     * @var string
     */
    private $outputFile;

    public function __construct()
    {
        $this->version    = null;
        $this->sourceFile = null;
        $this->outputFile = null;
    }

    /**
     * 
     * @param string $version
     */
    public function setVersion($version)
    {
        if (!preg_match("/^\\d+\\.\\d+\\.\\d+$/", $version)) {
            throw new \InvalidArgumentException("Version {$version} is invalid");
        }
        $this->version = $version;
    }

    /**
     * 
     * @return string
     */
    public function getVersion()
    {
        return $this->version;
    }

    /**
     * 
     * @param string $sourceFile
     */
    public function setSourceFile($sourceFile)
    {
        $this->sourceFile = $sourceFile;
    }

    /**
     * 
     * @return string
     */
    public function getSourceFile()
    {
        return $this->sourceFile;
    }

    /**
     * 
     * @param string $outputFile
     */
    public function setOutputFile($outputFile)
    {
        $this->outputFile = $outputFile;
    }

    /**
     * 
     * @return string
     */
    public function getOutputFile()
    {
        $outputFile = $this->outputFile;
        return ($outputFile === null) ? "php.ini" : $outputFile;
    }
}
