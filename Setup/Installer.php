<?php

namespace Setup;

class Installer
{
    /**
     *
     * @var Config
     */
    private $config;

    /**
     *
     * @var Translator
     */
    private $translator;

    /**
     * 
     * @param Config $config
     */
    public function __construct(Config $config)
    {
        $this->config     = $config;
        $this->translator = new Translator($config);
    }

    /**
     * 
     * @param string $ver
     */
    public function install($ver)
    {
        $p = new TranslateParameters();
        $p->setVersion($ver);
        $p->setSourceFile($this->formatDefaultIniFile($ver));

        $zipList = glob($this->config->getArchiveDir() . "/php-{$ver}-Win32*.zip");
        if (empty($zipList)) {
            throw new \InvalidArgumentException("Version {$ver} Not Found");
        }

        $zipPath = $zipList[0];
        $install = $this->config->getInstallDir();
        Zip::extract($zipPath, $install, $ver);

        $this->translator->translate($p, false);
        $p->setOutputFile("phpx.ini");
        $this->translator->translate($p, true);
    }

    /**
     * 
     * @param  string $ver
     * @return string
     */
    private function formatDefaultIniFile($ver)
    {
        $versions = explode(".", $ver);
        $major    = intval($versions[0]);
        $minor    = intval($versions[1]);
        return ($major === 7 || ($major === 5 && 3 <= $minor)) ? "php.ini-development" : "php.ini-recommended";
    }
}
