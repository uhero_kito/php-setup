<?php

namespace Setup;

class Translator
{
    /**
     *
     * @var Config
     */
    private $config;

    /**
     * 
     * @param Config $config
     */
    public function __construct(Config $config)
    {
        $this->config = $config;
    }

    /**
     * 
     * @param TranslateParameters $p
     * @param bool $withXdebug
     */
    public function translate(TranslateParameters $p, $withXdebug = false)
    {
        $options = new TranslateOptions($p, $this->config);
        $result  = [];
        foreach (file($options->getSourcePath(), FILE_IGNORE_NEW_LINES) as $line) {
            $result[] = $this->handleLine($line, $options);
        }
        if ($withXdebug && null !== ($xdebug = $options->getXdebugPath())) {
            $result[] = "\nzend_extension = \"{$xdebug}\"";
            if (null !== ($xdebugIdeKey = $options->getXdebugIdeKey())) {
                $result[] = "xdebug.idekey  = \"{$xdebugIdeKey}\"";
            }
        }
        file_put_contents($options->getOutputPath(), implode("\n", $result) . "\n");
    }

    private function handleLine($line, TranslateOptions $options)
    {
        $errorLog = $options->getErrorLog();
        $ext      = $options->getExtensionDir();
        $timezone = $options->getTimezone();
        if ($line === ";error_log = syslog" && strlen($errorLog)) {
            return "{$line}\nerror_log = \"{$errorLog}\"";
        }
        if ($line === '; extension_dir = "ext"') {
            return "{$line}\nextension_dir = \"{$ext}\"";
        }
        if ($line === 'extension_dir = "./"') {
            return "extension_dir = \"{$ext}\"";
        }
        $detected = $this->detectExtension($line);
        if (strlen($detected)) {
            return $options->hasModule($detected) ? substr($line, 1) : $line;
        }
        if ($line === ";date.timezone =" && strlen($timezone)) {
            return "date.timezone = \"{$timezone}\"";
        }

        return $line;
    }

    /**
     * 
     * @param string $line
     * @return string
     */
    private function detectExtension($line)
    {
        $matched = [];
        if (preg_match("/^;extension=php_([a-zA-Z0-9_]+)\\.dll/", $line, $matched)) {
            return $matched[1];
        }
        if (preg_match("/^;extension=([a-zA-Z0-9_]+)/", $line, $matched)) {
            return $matched[1];
        }
        return "";
    }
}
