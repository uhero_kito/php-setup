<?php

namespace Setup;

class Config
{
    /**
     *
     * @var string
     */
    private $archiveDir;

    /**
     *
     * @var string
     */
    private $installDir;

    /**
     *
     * @var string
     */
    private $errorLog;

    /**
     *
     * @var array
     */
    private $modules;

    /**
     *
     * @var string
     */
    private $xdebugDir;

    /**
     *
     * @var string
     */
    private $xdebugIdeKey;

    /**
     * 
     * @param ConfigParameters $p
     */
    public function __construct(ConfigParameters $p)
    {
        $archiveDir   = $p->getArchiveDir();
        $installDir   = $p->getInstallDir();
        $errorLog     = $p->getErrorLog();
        $modules      = $p->getModules();
        $timezone     = $p->getTimezone();
        $xdebugDir    = $p->getXdebugDir();
        $xdebugIdeKey = $p->getXdebugIdeKey();
        if ($archiveDir === null) {
            throw new \InvalidArgumentException("Parameter archiveDir is required.");
        }
        if ($installDir === null) {
            throw new \InvalidArgumentException("Parameter installDir is required.");
        }

        $this->archiveDir   = $archiveDir;
        $this->installDir   = $installDir;
        $this->errorLog     = $errorLog;
        $this->modules      = $modules;
        $this->timezone     = $timezone;
        $this->xdebugDir    = $xdebugDir;
        $this->xdebugIdeKey = $xdebugIdeKey;
    }

    /**
     * @return string
     */
    public function getArchiveDir()
    {
        return $this->archiveDir;
    }

    /**
     * @return string
     */
    public function getInstallDir()
    {
        return $this->installDir;
    }

    /**
     * @return string
     */
    public function getErrorLog()
    {
        return $this->errorLog;
    }

    /**
     * 
     * @return array
     */
    public function getModules()
    {
        return $this->modules;
    }

    /**
     * 
     * @return string
     */
    public function getTimezone()
    {
        return $this->timezone;
    }

    /**
     * 
     * @return string
     */
    public function getXdebugDir()
    {
        return $this->xdebugDir;
    }

    /**
     * 
     * @return string
     */
    public function getXdebugIdeKey()
    {
        return $this->xdebugIdeKey;
    }
}
