<?php

namespace Setup;

class ConfigParameters
{
    /**
     *
     * @var string
     */
    private $archiveDir;

    /**
     *
     * @var string
     */
    private $installDir;

    /**
     *
     * @var string
     */
    private $errorLog;

    /**
     *
     * @var array
     */
    private $modules;

    /**
     *
     * @var string
     */
    private $timezone;

    /**
     *
     * @var string
     */
    private $xdebugDir;

    /**
     *
     * @var string
     */
    private $xdebugIdeKey;

    public function __construct()
    {
        $this->archiveDir   = null;
        $this->installDir   = null;
        $this->errorLog     = null;
        $this->modules      = [];
        $this->timezone     = null;
        $this->xdebugDir    = null;
        $this->xdebugIdeKey = null;
    }

    /**
     * 
     * @param string $archiveDir
     */
    public function setArchiveDir($archiveDir)
    {
        if (!is_dir($archiveDir)) {
            throw new \InvalidArgumentException("Directory {$archiveDir} Not Found");
        }
        $this->archiveDir = $archiveDir;
    }

    /**
     * @return string
     */
    public function getArchiveDir()
    {
        return $this->archiveDir;
    }

    /**
     * 
     * @param string $installDir
     */
    public function setInstallDir($installDir)
    {
        if (!is_dir($installDir)) {
            throw new \InvalidArgumentException("Directory {$installDir} Not Found");
        }
        $this->installDir = $installDir;
    }

    /**
     * @return string
     */
    public function getInstallDir()
    {
        return $this->installDir;
    }

    /**
     * 
     * @param string $errorLog
     */
    public function setErrorLog($errorLog)
    {
        $this->errorLog = $errorLog;
    }

    /**
     * 
     * @return string
     */
    public function getErrorLog()
    {
        return $this->errorLog;
    }

    /**
     * 
     * @param string $module
     */
    public function addModule($module)
    {
        $this->modules[] = $module;
    }

    /**
     * 
     * @return array
     */
    public function getModules()
    {
        return $this->modules;
    }

    /**
     * 
     * @param string $timezone
     */
    public function setTimezone($timezone)
    {
        $this->timezone = $timezone;
    }

    /**
     * 
     * @return string
     */
    public function getTimezone()
    {
        return $this->timezone;
    }

    /**
     * 
     * @param string $xdebugDir
     */
    public function setXdebugDir($xdebugDir)
    {
        $notEmpty = boolval(strlen($xdebugDir));
        if ($notEmpty && !is_dir($xdebugDir)) {
            throw new \InvalidArgumentException("Directory {$xdebugDir} Not Found");
        }
        $this->xdebugDir = $notEmpty ? $xdebugDir : null;
    }

    /**
     * 
     * @return string
     */
    public function getXdebugDir()
    {
        return $this->xdebugDir;
    }

    /**
     * 
     * @param string $xdebugIdeKey
     */
    public function setXdebugIdeKey($xdebugIdeKey)
    {
        $this->xdebugIdeKey = $xdebugIdeKey;
    }

    /**
     * 
     * @return string
     */
    public function getXdebugIdeKey()
    {
        return $this->xdebugIdeKey;
    }
}
