<?php

namespace Setup;

class TranslateOptions
{
    /**
     *
     * @var string
     */
    private $sourcePath;

    /**
     *
     * @var string
     */
    private $outputPath;

    /**
     *
     * @var string
     */
    private $extensionDir;

    /**
     *
     * @var string
     */
    private $xdebugPath;

    /**
     *
     * @var Config
     */
    private $config;

    /**
     * 
     * @param TranslateParameters $p
     * @param Config $config
     */
    public function __construct(TranslateParameters $p, Config $config)
    {
        if (null === ($version = $p->getVersion())) {
            throw new \InvalidArgumentException("Version not specified");
        }
        if (null === ($sourceFile = $p->getSourceFile())) {
            throw new \InvalidArgumentException("Source file not specified");
        }
        $base       = $config->getInstallDir() . "/{$version}";
        $sourcePath = "{$base}/{$sourceFile}";
        if (!file_exists($sourcePath)) {
            throw new \InvalidArgumentException("Source File '{$sourcePath}' Not Found");
        }

        $this->sourcePath   = $sourcePath;
        $this->outputPath   = "{$base}/" . $p->getOutputFile();
        $this->extensionDir = "{$base}/ext";
        $this->xdebugPath   = $this->detectXdebugPath($config->getXdebugDir(), $version);
        $this->config       = $config;
    }

    /**
     * 
     * @return string
     */
    public function getSourcePath()
    {
        return $this->sourcePath;
    }

    /**
     * 
     * @return string
     */
    public function getOutputPath()
    {
        return $this->outputPath;
    }

    /**
     * 
     * @return string
     */
    public function getExtensionDir()
    {
        return $this->extensionDir;
    }

    /**
     * 
     * @return string
     */
    public function getErrorLog()
    {
        return $this->config->getErrorLog();
    }

    /**
     * 
     * @param string $module
     * @return boolean
     */
    public function hasModule($module)
    {
        return in_array($module, $this->config->getModules());
    }

    /**
     * 
     * @return string
     */
    public function getTimezone()
    {
        return $this->config->getTimezone();
    }

    /**
     * 
     * @param  string $dir
     * @param  string $ver
     * @return string
     */
    private function detectXdebugPath($dir, $ver)
    {
        if ($dir === null) {
            return null;
        }

        $i       = new \DirectoryIterator($dir);
        $latest  = null;
        $matched = [];
        foreach ($i as $file) {
            if (!$file->isFile()) {
                continue;
            }
            $filename = $file->getFilename();
            if (!preg_match("/^php_xdebug-(\\d+\\.\\d+\\.\\d+)(rc\\d+)?-(\\d+\\.\\d+)-vc(\\d+)\\.dll$/", $filename, $matched)) {
                continue;
            }
            $targetVer = $matched[3];
            if (substr($ver, 0, strlen($targetVer)) !== $targetVer) {
                continue;
            }
            if ($latest === null) {
                $latest = $matched;
                continue;
            }
            if (0 < strnatcmp($matched[1], $latest[1])) {
                $latest = $matched;
                continue;
            }
            if (0 < strnatcmp($matched[4], $latest[4])) {
                $latest = $matched;
                continue;
            }
        }
        return $latest ? $dir . "/" . $latest[0] : null;
    }

    /**
     * 
     * @return string
     */
    public function getXdebugPath()
    {
        return $this->xdebugPath;
    }

    /**
     * 
     * @return string
     */
    public function getXdebugIdeKey()
    {
        return $this->config->getXdebugIdeKey();
    }
}
