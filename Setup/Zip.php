<?php

namespace Setup;

class Zip
{

    public static function extract($zipPath, $destDir, $newDirname)
    {
        if (!is_dir($destDir)) {
            throw new \InvalidArgumentException("Directory {$destDir} Not Found");
        }

        $zip  = new \ZipArchive();
        $zip->open($zipPath);
        $dest = "{$destDir}/{$newDirname}";
        @mkdir($dest);
        $zip->extractTo($dest);
        $zip->close();
    }
}
