<?php

namespace Setup;

class TranslateOptionsTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var TranslateOptions
     */
    protected $object;

    /**
     *
     * @var Config
     */
    private $config;

    /**
     * Sets up the fixture, for example, opens a network connection.
     * This method is called before a test is executed.
     */
    protected function setUp()
    {
        $p1 = new TranslateParameters();
        $p1->setVersion("5.2.99");
        $p1->setSourceFile("test01-before.ini");
        $p1->setOutputFile("test01-translated.ini");

        $p2 = new ConfigParameters();
        $p2->setArchiveDir(TEST_ARCHIVE_DIR);
        $p2->setInstallDir(TEST_TEMPLATE_DIR);
        $p2->setErrorLog(TEST_ERROR_LOG);
        $p2->addModule("curl");
        $p2->addModule("mbstring");
        $p2->addModule("openssl");
        $p2->addModule("pdo_mysql");
        $p2->setTimezone("Asia/Tokyo");
        $p2->setXdebugIdeKey("netbeans-xdebug");

        $this->config = new Config($p2);
        $this->object = new TranslateOptions($p1, $this->config);
    }

    /**
     * Tears down the fixture, for example, closes a network connection.
     * This method is called after a test is executed.
     */
    protected function tearDown()
    {
        
    }

    /**
     * @covers Setup\TranslateOptions::__construct
     * @expectedException \InvalidArgumentException
     */
    public function test__constructFailIfEmptyVersion()
    {
        $p1 = new TranslateParameters();
        $p1->setSourceFile("test01-before.ini");
        $p1->setOutputFile("test01-translated.ini");
        new TranslateOptions($p1, $this->config);
    }

    /**
     * @covers Setup\TranslateOptions::__construct
     * @expectedException \InvalidArgumentException
     */
    public function test__constructFailIfEmptySourceFile()
    {
        $p1 = new TranslateParameters();
        $p1->setVersion("5.2.99");
        $p1->setOutputFile("test01-translated.ini");
        new TranslateOptions($p1, $this->config);
    }

    /**
     * @covers Setup\TranslateOptions::__construct
     * @expectedException \InvalidArgumentException
     */
    public function test__constructFailIfSourceFileNotFound()
    {
        $p1 = new TranslateParameters();
        $p1->setVersion("5.2.66"); // Not exists
        $p1->setSourceFile("test01-before.ini");
        $p1->setOutputFile("test01-translated.ini");
        new TranslateOptions($p1, $this->config);
    }

    /**
     * @covers Setup\TranslateOptions::getSourcePath
     */
    public function testGetSourcePath()
    {
        $obj = $this->object;
        $this->assertSame(TEST_TEMPLATE_DIR . "/5.2.99/test01-before.ini", $obj->getSourcePath());
    }

    /**
     * @covers Setup\TranslateOptions::getOutputPath
     */
    public function testGetOutputPath()
    {
        $obj = $this->object;
        $this->assertSame(TEST_TEMPLATE_DIR . "/5.2.99/test01-translated.ini", $obj->getOutputPath());
    }

    /**
     * @covers Setup\TranslateOptions::__construct
     * @covers Setup\TranslateOptions::getOutputPath
     */
    public function testGetOutputPathByDefault()
    {
        $p1 = new TranslateParameters();
        $p1->setVersion("5.2.99");
        $p1->setSourceFile("test01-before.ini");

        $obj = new TranslateOptions($p1, $this->config);
        $this->assertSame(TEST_TEMPLATE_DIR . "/5.2.99/php.ini", $obj->getOutputPath());
    }

    /**
     * @covers Setup\TranslateOptions::getExtensionDir
     */
    public function testGetExtensionDir()
    {
        $obj = $this->object;
        $this->assertSame(TEST_TEMPLATE_DIR . "/5.2.99/ext", $obj->getExtensionDir());
    }

    /**
     * @covers Setup\TranslateOptions::getErrorLog
     */
    public function testGetErrorLog()
    {
        $obj = $this->object;
        $this->assertSame(TEST_ERROR_LOG, $obj->getErrorLog());
    }

    /**
     * @covers Setup\TranslateOptions::hasModule
     */
    public function testHasModule()
    {
        $obj = $this->object;
        $this->assertTrue($obj->hasModule("mbstring"));
        $this->assertFalse($obj->hasModule("pdo_sqlite"));
    }

    /**
     * @covers Setup\TranslateOptions::getTimezone
     */
    public function testGetTimezone()
    {
        $obj = $this->object;
        $this->assertSame("Asia/Tokyo", $obj->getTimezone());
    }

    /**
     * @covers Setup\TranslateOptions::getXdebugPath
     * @covers Setup\TranslateOptions::detectXdebugPath
     */
    public function testGetXdebugPath()
    {
        $cp = new ConfigParameters();
        $cp->setArchiveDir(TEST_ARCHIVE_DIR);
        $cp->setInstallDir(TEST_TEMPLATE_DIR);
        $cp->setErrorLog(TEST_ERROR_LOG);
        $tp = new TranslateParameters();
        $tp->setSourceFile("test01-before.ini");
        $tp->setVersion("5.5.99");

        $cp->setXdebugDir(TEST_TEMPLATE_DIR . "/xdebug-test01");
        $obj1 = new TranslateOptions($tp, new Config($cp));
        $this->assertSame(TEST_TEMPLATE_DIR . "/xdebug-test01/php_xdebug-2.3.2-5.5-vc11.dll", $obj1->getXdebugPath());

        $cp->setXdebugDir(TEST_TEMPLATE_DIR . "/xdebug-test02");
        $obj2 = new TranslateOptions($tp, new Config($cp));
        $this->assertNull($obj2->getXdebugPath());

        $cp->setXdebugDir("");
        $obj3 = new TranslateOptions($tp, new Config($cp));
        $this->assertNull($obj3->getXdebugPath());

        $cp->setXdebugDir(TEST_TEMPLATE_DIR . "/xdebug-test01");
        $tp2  = new TranslateParameters();
        $tp2->setVersion("7.0.99");
        $tp2->setSourceFile("test01-before.ini");
        $obj4 = new TranslateOptions($tp2, new Config($cp));
        $this->assertSame(TEST_TEMPLATE_DIR . "/xdebug-test01/php_xdebug-2.4.0rc3-7.0-vc14.dll", $obj4->getXdebugPath());
    }

    /**
     * @covers Setup\TranslateOptions::getXdebugIdeKey
     */
    public function testGetXdebugIdeKey()
    {
        $obj = $this->object;
        $this->assertSame("netbeans-xdebug", $obj->getXdebugIdeKey());
    }
}
