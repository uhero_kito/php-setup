<?php

namespace Setup;

class InstallerTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var Installer
     */
    protected $object;

    /**
     * Sets up the fixture, for example, opens a network connection.
     * This method is called before a test is executed.
     */
    protected function setUp()
    {
        $p = new ConfigParameters();
        $p->setArchiveDir(TEST_ARCHIVE_DIR);
        $p->setInstallDir(TEST_INSTALL_DIR);
        $p->setErrorLog(TEST_ERROR_LOG);
        $p->addModule("curl");
        $p->addModule("mbstring");
        $p->addModule("openssl");
        $p->addModule("pdo_mysql");
        $p->setTimezone("Asia/Tokyo");
        $p->setXdebugDir(TEST_XDEBUG_DIR);
        $p->setXdebugIdeKey("netbeans-xdebug");

        $this->object = new Installer(new Config($p));
    }

    /**
     * Tears down the fixture, for example, closes a network connection.
     * This method is called after a test is executed.
     */
    protected function tearDown()
    {
        
    }

    /**
     * @covers Setup\Installer::__construct
     * @covers Setup\Installer::install
     * @covers Setup\Installer::formatDefaultIniFile
     */
    public function testInstall()
    {
        $obj = $this->object;

        $obj->install("5.3.0");
        $expected1 = TEST_TEMPLATE_DIR . "/5.3.0/expected.ini";
        $this->assertFileEquals($expected1, TEST_INSTALL_DIR . "/5.3.0/phpx.ini");

        $obj->install("5.2.0");
        $expected2 = TEST_TEMPLATE_DIR . "/5.2.0/expected.ini";
        $this->assertFileEquals($expected2, TEST_INSTALL_DIR . "/5.2.0/php.ini");
    }

    /**
     * @covers Setup\Installer::install
     * @expectedException \InvalidArgumentException
     */
    public function testInstallFailByNoVersion()
    {
        $obj = $this->object;
        $obj->install("5.1.9");
    }

    /**
     * @covers Setup\Installer::install
     * @expectedException \InvalidArgumentException
     */
    public function testInstallFailByInvalidVersion()
    {
        $obj = $this->object;
        $obj->install("invalid");
    }
}
