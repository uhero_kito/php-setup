<?php

namespace Setup;

class TranslatorTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var Translator
     */
    protected $object;

    /**
     * Sets up the fixture, for example, opens a network connection.
     * This method is called before a test is executed.
     */
    protected function setUp()
    {
        $p = new ConfigParameters();
        $p->setArchiveDir(TEST_ARCHIVE_DIR);
        $p->setInstallDir(TEST_TEMPLATE_DIR);
        $p->setErrorLog(TEST_ERROR_LOG);
        $p->addModule("curl");
        $p->addModule("mbstring");
        $p->addModule("openssl");
        $p->addModule("pdo_mysql");
        $p->setTimezone("Asia/Tokyo");
        $p->setXdebugDir(TEST_XDEBUG_DIR);

        $this->object = new Translator(new Config($p));
    }

    /**
     * Tears down the fixture, for example, closes a network connection.
     * This method is called after a test is executed.
     */
    protected function tearDown()
    {
        
    }

    /**
     * @covers Setup\Translator::__construct
     * @covers Setup\Translator::translate
     * @covers Setup\Translator::handleLine
     * @covers Setup\Translator::detectExtension
     */
    public function testTranslate()
    {
        $tests = ["test01", "test02", "test03", "test04", "test05"];
        $p     = new TranslateParameters();
        $p->setVersion("5.2.99");
        foreach ($tests as $t) {
            $p->setSourceFile("{$t}-before.ini");
            $p->setOutputFile("{$t}-translated.ini");
            $this->object->translate($p);

            $expected   = TEST_TEMPLATE_DIR . "/5.2.99/{$t}-expected.ini";
            $translated = TEST_TEMPLATE_DIR . "/5.2.99/{$t}-translated.ini";
            $this->assertFileEquals($expected, $translated);
        }
    }

    /**
     * @covers Setup\Translator::__construct
     * @covers Setup\Translator::translate
     * @covers Setup\Translator::handleLine
     * @covers Setup\Translator::detectExtension
     */
    public function testTranslate7_2()
    {
        $p = new TranslateParameters();
        $p->setVersion("7.2.99");
        $p->setSourceFile("test01-before.ini");
        $p->setOutputFile("test01-translated.ini");
        $this->object->translate($p);

        $expected   = TEST_TEMPLATE_DIR . "/7.2.99/test01-expected.ini";
        $translated = TEST_TEMPLATE_DIR . "/7.2.99/test01-translated.ini";
        $this->assertFileEquals($expected, $translated);
    }

    /**
     * @covers Setup\Translator::__construct
     * @covers Setup\Translator::translate
     * @covers Setup\Translator::handleLine
     */
    public function testTranslateXdebug()
    {
        $p = new TranslateParameters();
        $p->setVersion("5.5.99");
        $p->setSourceFile("test01-before.ini");
        $p->setOutputFile("test01-translated.ini");
        $this->object->translate($p, true);

        $expected   = TEST_TEMPLATE_DIR . "/5.5.99/test01-expected.ini";
        $translated = TEST_TEMPLATE_DIR . "/5.5.99/test01-translated.ini";
        $this->assertFileEquals($expected, $translated);
    }

    /**
     * @covers Setup\Translator::translate
     * @expectedException \InvalidArgumentException
     */
    public function testTranslateFailIfFileNotFound()
    {
        $p = new TranslateParameters();
        $p->setVersion("5.2.99");
        $p->setSourceFile("notfound.ini");
        $p->setOutputFile("test01-translated.ini");
        $this->object->translate($p);
    }
}
