<?php

namespace Setup;

class TranslateParametersTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var TranslateParameters
     */
    protected $object;

    /**
     * Sets up the fixture, for example, opens a network connection.
     * This method is called before a test is executed.
     */
    protected function setUp()
    {
        $this->object = new TranslateParameters();
    }

    /**
     * Tears down the fixture, for example, closes a network connection.
     * This method is called after a test is executed.
     */
    protected function tearDown()
    {
        
    }

    /**
     * @covers Setup\TranslateParameters::__construct
     */
    public function test__construct()
    {
        $obj = new TranslateParameters();
        $this->assertNull($obj->getVersion());
        $this->assertNull($obj->getSourceFile());
        $this->assertSame("php.ini", $obj->getOutputFile());
    }

    /**
     * @covers Setup\TranslateParameters::getVersion
     * @covers Setup\TranslateParameters::setVersion
     */
    public function testAccessVersion()
    {
        $obj = $this->object;
        $obj->setVersion("5.3.0");
        $this->assertSame("5.3.0", $obj->getVersion());
    }

    /**
     * @covers Setup\TranslateParameters::setVersion
     * @expectedException \InvalidArgumentException
     */
    public function testSetVersionFailed()
    {
        $obj = $this->object;
        $obj->setVersion("invalid");
    }

    /**
     * @covers Setup\TranslateParameters::getSourceFile
     * @covers Setup\TranslateParameters::setSourceFile
     */
    public function testAccessSourceFile()
    {
        $obj = $this->object;
        $arg = "test01-before.ini";
        $obj->setSourceFile($arg);
        $this->assertSame($arg, $obj->getSourceFile());
    }

    /**
     * @covers Setup\TranslateParameters::getOutputFile
     * @covers Setup\TranslateParameters::setOutputFile
     */
    public function testAccessOutputFile()
    {
        $obj = $this->object;
        $arg = "test01-translated.ini";
        $obj->setOutputFile($arg);
        $this->assertSame($arg, $obj->getOutputFile());
    }
}
