<?php

namespace Setup;

class ConfigTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var Config
     */
    protected $object;

    /**
     * Sets up the fixture, for example, opens a network connection.
     * This method is called before a test is executed.
     */
    protected function setUp()
    {
        $p = new ConfigParameters();
        $p->setArchiveDir(TEST_ARCHIVE_DIR);
        $p->setInstallDir(TEST_INSTALL_DIR);
        $p->setErrorLog(TEST_ERROR_LOG);
        $p->addModule("curl");
        $p->addModule("mbstring");
        $p->addModule("openssl");
        $p->addModule("pdo_mysql");
        $p->setTimezone("Asia/Tokyo");
        $p->setXdebugDir(TEST_XDEBUG_DIR);
        $p->setXdebugIdeKey("netbeans-xdebug");

        $this->object = new Config($p);
    }

    /**
     * Tears down the fixture, for example, closes a network connection.
     * This method is called after a test is executed.
     */
    protected function tearDown()
    {
        
    }

    /**
     * @covers Setup\Config::__construct
     * @expectedException \InvalidArgumentException
     */
    public function test__constructFailIfEmptyArchiveDir()
    {
        $p = new ConfigParameters();
        $p->setInstallDir(TEST_INSTALL_DIR);
        new Config($p);
    }

    /**
     * @covers Setup\Config::__construct
     * @expectedException \InvalidArgumentException
     */
    public function test__constructFailIfEmptyInstallDir()
    {
        $p = new ConfigParameters();
        $p->setArchiveDir(TEST_ARCHIVE_DIR);
        new Config($p);
    }

    /**
     * @covers Setup\Config::getArchiveDir
     */
    public function testGetArchiveDir()
    {
        $this->assertSame(TEST_ARCHIVE_DIR, $this->object->getArchiveDir());
    }

    /**
     * @covers Setup\Config::getInstallDir
     */
    public function testGetInstallDir()
    {
        $this->assertSame(TEST_INSTALL_DIR, $this->object->getInstallDir());
    }

    /**
     * @covers Setup\Config::getErrorLog
     */
    public function testGetErrorLog()
    {
        $this->assertSame(TEST_ERROR_LOG, $this->object->getErrorLog());
    }

    /**
     * @covers Setup\Config::getModules
     */
    public function testGetModules()
    {
        $expected = ["curl", "mbstring", "openssl", "pdo_mysql"];
        $this->assertSame($expected, $this->object->getModules());
    }

    /**
     * @covers Setup\Config::getTimezone
     */
    public function testGetTimezone()
    {
        $this->assertSame("Asia/Tokyo", $this->object->getTimezone());
    }

    /**
     * @covers Setup\Config::getXdebugDir
     */
    public function testGetXdebugDir()
    {
        $this->assertSame(TEST_XDEBUG_DIR, $this->object->getXdebugDir());
    }

    /**
     * @covers Setup\Config::getXdebugIdeKey
     */
    public function testGetXdebugIdeKey()
    {
        $this->assertSame("netbeans-xdebug", $this->object->getXdebugIdeKey());
    }
}
