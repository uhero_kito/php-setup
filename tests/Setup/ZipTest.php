<?php

namespace Setup;

class ZipTest extends \PHPUnit_Framework_TestCase
{

    /**
     * Sets up the fixture, for example, opens a network connection.
     * This method is called before a test is executed.
     */
    protected function setUp()
    {
        
    }

    /**
     * Tears down the fixture, for example, closes a network connection.
     * This method is called after a test is executed.
     */
    protected function tearDown()
    {
        
    }

    /**
     * @covers Setup\Zip::extract
     */
    public function testExtract()
    {
        Zip::extract(TEST_ARCHIVE_DIR . "/zip-sample.zip", TEST_TEMP_DIR, "zip-sample");
        $testFile = TEST_TEMP_DIR . "/zip-sample/dir01/test03.txt";
        $this->assertFileExists($testFile);
        $this->assertSame("Sample", file_get_contents($testFile));
    }

    /**
     * @covers Setup\Zip::extract
     * @expectedException \InvalidArgumentException
     */
    public function testExtractFail()
    {
        Zip::extract(TEST_ARCHIVE_DIR . "/zip-sample.zip", __DIR__ . "/not-found", "zip-sample");
    }
}
