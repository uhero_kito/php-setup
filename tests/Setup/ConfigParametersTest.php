<?php

namespace Setup;

class ConfigParametersTest extends \PHPUnit_Framework_TestCase
{
    /**
     * @var ConfigParameters
     */
    protected $object;

    /**
     * Sets up the fixture, for example, opens a network connection.
     * This method is called before a test is executed.
     */
    protected function setUp()
    {
        $this->object = new ConfigParameters();
    }

    /**
     * Tears down the fixture, for example, closes a network connection.
     * This method is called after a test is executed.
     */
    protected function tearDown()
    {
        
    }

    /**
     * @covers Setup\ConfigParameters::__construct
     */
    public function test__Construct()
    {
        $obj = new ConfigParameters();
        $this->assertNull($obj->getArchiveDir());
        $this->assertNull($obj->getInstallDir());
        $this->assertNull($obj->getTimezone());
    }

    /**
     * @covers Setup\ConfigParameters::setArchiveDir
     * @covers Setup\ConfigParameters::getArchiveDir
     */
    public function testAccessArchiveDir()
    {
        $obj = $this->object;
        $obj->setArchiveDir(TEST_ARCHIVE_DIR);
        $this->assertSame(TEST_ARCHIVE_DIR, $obj->getArchiveDir());
    }

    /**
     * @covers Setup\ConfigParameters::setInstallDir
     * @covers Setup\ConfigParameters::getInstallDir
     */
    public function testAccessInstallDir()
    {
        $obj = $this->object;
        $obj->setInstallDir(TEST_INSTALL_DIR);
        $this->assertSame(TEST_INSTALL_DIR, $obj->getInstallDir());
    }

    /**
     * @covers Setup\ConfigParameters::setArchiveDir
     * @expectedException \InvalidArgumentException
     */
    public function testArchiveDirValidation()
    {
        $obj = $this->object;
        $obj->setArchiveDir(__DIR__ . "/notfound");
    }

    /**
     * @covers Setup\ConfigParameters::setInstallDir
     * @expectedException \InvalidArgumentException
     */
    public function testInstallDirValidation()
    {
        $obj = $this->object;
        $obj->setInstallDir(__DIR__ . "/notfound");
    }

    /**
     * @covers Setup\ConfigParameters::setErrorLog
     * @covers Setup\ConfigParameters::getErrorLog
     */
    public function testAccessErrorLog()
    {
        $obj = $this->object;
        $obj->setErrorLog(TEST_ERROR_LOG);
        $this->assertSame(TEST_ERROR_LOG, $obj->getErrorLog());
    }

    /**
     * @covers Setup\ConfigParameters::addModule
     * @covers Setup\ConfigParameters::getModules
     */
    public function testAccessModules()
    {
        $obj = $this->object;
        $obj->addModule("curl");
        $obj->addModule("mbstring");
        $obj->addModule("openssl");
        $obj->addModule("pdo_mysql");
        $this->assertSame(["curl", "mbstring", "openssl", "pdo_mysql"], $obj->getModules());
    }

    /**
     * @covers Setup\ConfigParameters::setTimezone
     * @covers Setup\ConfigParameters::getTimezone
     */
    public function testAccessTimezone()
    {
        $obj = $this->object;
        $obj->setTimezone("Asia/Tokyo");
        $this->assertSame("Asia/Tokyo", $obj->getTimezone());
    }

    /**
     * @covers Setup\ConfigParameters::setXdebugDir
     * @covers Setup\ConfigParameters::getXdebugDir
     */
    public function testAccessXdebugDir()
    {
        $obj = $this->object;
        $obj->setXdebugDir(TEST_XDEBUG_DIR);
        $this->assertSame(TEST_XDEBUG_DIR, $obj->getXdebugDir());
        $obj->setXdebugDir("");
        $this->assertNull($obj->getXdebugDir());
    }

    /**
     * @covers Setup\ConfigParameters::setXdebugDir
     * @expectedException \InvalidArgumentException
     */
    public function testXdebugDirValidation()
    {
        $obj = $this->object;
        $obj->setXdebugDir(__DIR__ . "/notfound");
    }

    /**
     * @covers Setup\ConfigParameters::setXdebugIdeKey
     * @covers Setup\ConfigParameters::getXdebugIdeKey
     */
    public function testAccessXdebugIdeKey()
    {
        $obj = $this->object;
        $obj->setXdebugIdeKey("netbeans-xdebug");
        $this->assertSame("netbeans-xdebug", $obj->getXdebugIdeKey());
    }
}
