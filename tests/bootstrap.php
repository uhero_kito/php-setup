<?php

require_once(dirname(__DIR__) . "/autoload.php");

$bootstrap = function () {
    $fileIterator = function ($dir) {
        if (!is_dir($dir)) {
            return new EmptyIterator();
        }

        $r = new RecursiveDirectoryIterator($dir, FilesystemIterator::KEY_AS_PATHNAME | FilesystemIterator::CURRENT_AS_FILEINFO | FilesystemIterator::SKIP_DOTS);
        return new RecursiveIteratorIterator($r, RecursiveIteratorIterator::CHILD_FIRST);
    };

    $isWin  = (substr(PHP_OS, 0, 3) === "WIN");
    $tmpDir = $isWin ? "C:/Temp/php-setup" : "/tmp/php-setup";
    define("TEST_TEMP_DIR", $tmpDir);
    define("TEST_ARCHIVE_DIR", "{$tmpDir}/archive");
    define("TEST_INSTALL_DIR", "{$tmpDir}/install");
    define("TEST_TEMPLATE_DIR", "{$tmpDir}/template");
    define("TEST_ERROR_LOG", "{$tmpDir}/php.log");
    define("TEST_XDEBUG_DIR", TEST_TEMPLATE_DIR . "/xdebug");

    foreach ($fileIterator(TEST_TEMP_DIR) as $file) {
        $path = $file->getPathname();
        if ($file->isDir()) {
            rmdir($path);
        } else {
            unlink($path);
        }
    }
    @mkdir(TEST_TEMP_DIR, 0777, true);
    @mkdir(TEST_ARCHIVE_DIR, 0777, true);
    @mkdir(TEST_INSTALL_DIR, 0777, true);
    @mkdir(TEST_TEMPLATE_DIR, 0777, true);

    $initZip = function ($dirname) use ($fileIterator) {
        $zip          = new ZipArchive();
        $zip->open(TEST_ARCHIVE_DIR . "/{$dirname}.zip", \ZipArchive::CREATE);
        $prefix       = __DIR__ . "/archive/{$dirname}";
        $prefixLength = strlen($prefix) + 1;
        foreach ($fileIterator($prefix) as $file) {
            $target    = $file->getPathname();
            $localname = str_replace("\\", "/", substr($target, $prefixLength));
            if ($file->isFile()) {
                $zip->addFile($target, $localname);
            }
        }
        $zip->close();
    };
    foreach (new DirectoryIterator(__DIR__ . "/archive") as $file) {
        if ($file->isDir() && !$file->isDot()) {
            $initZip($file->getFilename());
        }
    }

    $templateBase = __DIR__ . "/template";
    $initTemplate = function ($filename) use ($templateBase) {
        $fullpath = "{$templateBase}/{$filename}";
        if (substr($filename, -5) !== ".dist") {
            copy($fullpath, TEST_TEMPLATE_DIR . "/{$filename}");
            return;
        }

        $contents      = file_get_contents($fullpath);
        $replace       = [
            "{TEST_ARCHIVE_DIR}"  => TEST_ARCHIVE_DIR,
            "{TEST_INSTALL_DIR}"  => TEST_INSTALL_DIR,
            "{TEST_TEMPLATE_DIR}" => TEST_TEMPLATE_DIR,
            "{TEST_ERROR_LOG}"    => TEST_ERROR_LOG,
            "{TEST_XDEBUG_DIR}"   => TEST_XDEBUG_DIR,
        ];
        $contentsAfter = strtr($contents, $replace);

        $filenameAfter = substr($filename, 0, -5);
        file_put_contents(TEST_TEMPLATE_DIR . "/{$filenameAfter}", $contentsAfter);
    };

    $digTemplate = function ($dir) use ($templateBase, $initTemplate, &$digTemplate) {
        foreach (new DirectoryIterator("{$templateBase}/{$dir}") as $file) {
            $prefix = strlen($dir) ? "{$dir}/" : "";
            $entry  = $prefix . $file->getFilename();
            if ($file->isFile()) {
                $initTemplate($entry);
            } elseif (!$file->isDot()) {
                @mkdir(TEST_TEMPLATE_DIR . "/{$entry}");
                $digTemplate($entry);
            }
        }
    };
    $digTemplate("");
};

call_user_func($bootstrap);
